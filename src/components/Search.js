import React from 'react';
import '../Search.css';
import axios from 'axios';
import Loader from '../loader.gif';
import PageNavigation from './PageNavigation';
import UnSplash from '../ApiIntace';
import { toJson } from "unsplash-js";
import debounce from 'lodash.debounce';
import Modal from './Modal';

class Search extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			query: '',
			results: [],
			loading: false,
			message: '',
			totalResults: 0,
			totalPages: 0,
			currentPageNo: 0,
			isVisible: false,
			selectetedImage: null
		};
		this.apiCall = debounce(this.fetchSearchResults, 500)
	}


	/**
	 * Get the Total Pages count.
	 *
	 * @param total
	 * @param denominator Count of results per page
	 * @return {number}
	 */
	getPageCount = (total, denominator) => {
		const divisible = 0 === total % denominator;
		const valueToBeAdded = divisible ? 0 : 1;
		return Math.floor(total / denominator) + valueToBeAdded;
	};

	/**
	 * Fetch the search results and update the state with the result.
	 * 
	 *
	 * @param {int} updatedPageNo Updated Page No.
	 * @param {String} query Search Query.
	 *
	 */
	fetchSearchResults = (updatedPageNo = 1, query) => {

		UnSplash.search.photos(query, updatedPageNo, 9, { orientation: "portrait" })
			.then(toJson)
			.then(res => {
				console.log(res);
				const total = res.total;
				const totalPagesCount = res.total_pages;
				const resultNotFoundMsg = !total
					? 'There are no more search results. Please try a new search'
					: '';
				let finalResults = res.results;
				if (updatedPageNo !== 1) {
					finalResults = this.state.results.concat(finalResults);
				}
				console.log('aaaaaaaaaa', finalResults)
				this.setState({
					results: finalResults,
					message: resultNotFoundMsg,
					totalResults: total,
					totalPages: totalPagesCount,
					currentPageNo: updatedPageNo,
					loading: false
				})
			});
	};

	handleOnInputChange = (event) => {
		const query = event.target.value;
		if (!query) {
			this.setState({ query, results: [], message: '', totalPages: 0, totalResults: 0 });
		} else {
			this.setState({ query, loading: true, message: '', results: [] }, () => {
				this.apiCall(1, query);
			});
		}
	};

	/**
	 * Fetch results according to the prev or next page requests.
	 *
	 * @param {String} type 'prev' or 'next'
	 */
	handlePageClick = (type) => {
		event.preventDefault();
		const updatePageNo = this.state.currentPageNo + 1;

		if (!this.state.loading) {
			this.setState({ loading: true, message: '' }, () => {
				this.apiCall(updatePageNo, this.state.query);
			});
		}
	};

	showModal = (selectetedImage) => {
		this.setState({
			isVisible: true,
			selectetedImage
		})
	}

	closeModal = () => {
		this.setState({
			isVisible: false,
			selectetedImage: null
		})
	}

	renderSearchResults = () => {
		const { results } = this.state;

		if (Object.keys(results).length && results.length) {
			return (
				<div className="results-container">
					{results.map((result, index) => {
						return (
							<a key={result.id} onClick={() => this.showModal(result)} className="result-item">
								{/* <h6 className="image-username">{result.user.first_name}</h6> */}
								<div className="image-username">
									<h5>
										<img src={result.user.profile_image.small} alt="Avatar" className="avatar" />
										  Image by <span className="green">{result.user.first_name} </span>
									</h5>
								</div>
								<div className="image-wrapper">
									<img className="image" src={result.urls.thumb} alt={`${result.user.first_name} image`} />
								</div>
							</a>
						)
					})}
				</div>
			)
		}
	};

	render() {
		const { query, loading, message, currentPageNo, totalPages, results } = this.state;

		const showNextLink = totalPages > currentPageNo;

		return (
			<div className="container">
				{/*	Heading*/}
				<h2 className="heading">React Application</h2>
				{/* Search Input*/}
				<label className="search-label" htmlFor="search-input">
					<input
						type="text"
						name="query"
						value={query}
						id="search-input"
						placeholder="search for image here..."
						onChange={this.handleOnInputChange}
					/>
					<i className="fa fa-search search-icon" aria-hidden="true" />
				</label>

				{/*	Error Message*/}
				{message && <p className="message">{message}</p>}

				{/*	Loader*/}
				<img src={Loader} className={`search-loading ${loading && results.length == 0 ? 'show' : 'hide'}`} alt="loader" />

				{/*Navigation*/}


				{/*	Result*/}
				{this.renderSearchResults()}

				{/*Navigation*/}
				<PageNavigation
					loading={loading}
					showNextLink={showNextLink}
					handleNextClick={() => this.handlePageClick(event)}
				/>
				<Modal
					isVisible={this.state.isVisible}
					onClose={this.closeModal}
					imageDatils={this.state.selectetedImage}
				/>

			</div>
		)
	}
}

export default Search
