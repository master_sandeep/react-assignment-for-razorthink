import React from 'react';
import unsplash from '../ApiIntace';
import { toJson } from "unsplash-js";

const Modal = ({ isVisible, imageDatils, onClose }) => {
    if (!isVisible) {
        return null;
    }
    const downloadImage = () => {
        const aaa = unsplash.photos.downloadPhoto(imageDatils).then((aaa) => {
            console.log('wwwwwwwwwwwwwww', aaa)

        });
    }

    return (
        <div id="myModal" className="modal" >

            <div className="modal-content">
                <span className="close" onClick={onClose}>&times;</span>
                <div className="image-username2" >
                    <h5>
                        <img src={imageDatils.user.profile_image.small} alt="Avatar" className="avatar2" />
                        <span className="green">{imageDatils.user.first_name} </span>
                    </h5>
                </div>
                <div className="image-wrapper">
                    <img className="full-image" src={imageDatils.urls.full} alt={"image"} />
                </div>
                <div className="nav-link-container">
                    <a className={`nav-link show`} href={imageDatils.links.download} download={imageDatils.id}>Downloads</a>
                </div>
            </div>
        </div>
    );
};


export default Modal;