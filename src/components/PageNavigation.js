import React from 'react';
import Loader from '../loader.gif';

export default (props) => {
	const {
		loading,
		showNextLink,
		handleNextClick,
	} = props;
	return (
		<div className="nav-link-container">
			{showNextLink && <> <a
				href="#"
				className={
					`nav-link 
					${ showNextLink ? 'show' : 'hide'}
					${ loading ? 'greyed-out' : ''}
					`}
				onClick={handleNextClick}
			>
				load more
			</a>
				{loading && <img src={Loader} className={`search-loading show `} alt="loader" />}
			</>
			}

		</div>
	)
}
